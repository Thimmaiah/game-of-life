// Conway's Game of Life ver 2.9.3
// (c) 2016, Dmytro Ievseienko
// Notice: jQuery is required

"use stict";

var GoL = {
    //- parameters/settings
    parameters:
    {
        v: '2.9.3',// version
        h: 0,      // amount of cells verically
        w: 0,      // amount of cells horizontally
        hpx: 440,  // canvas height [in px]
        wpx: 1138, // canvas width  [in px]
        zoom: 1,   // zoom mode
        speed: 5,  // speed of step [in ms]
        template: -1, // no template
        drawMode: 1,  // toggle
        fullHeight: false // vertical fullheight mode
    },
        
    // templates
    templates:
    [
        {//0
            name: 'Gliders',
            imgSrc: './img/gliders.png',
            descr: '4 Gliders flying away',
            zoom: 0,
            speed: 50,
            fieldShift: { x: -4, y: 0 },
            set: [[19, 54, 55, 61, 62], [20, 54, 56, 60, 62], [21, 54, 62], [27, 54, 62], [28, 54, 56, 60, 62], [29, 54, 55, 61, 62]]
        },
        {//1
            name: 'LWSS',
            imgSrc: './img/lwss.png',
            descr: 'The smallest known orthogonally moving spaceship, and the second most common (after the glider). Found by Conway in 1970',
            zoom: 0,
            speed: 50,
            fieldShift: { x: 0, y: 0 },
            set: [[20, 11, 14], [21, 15], [22, 11, 15], [23, 12, 13, 14, 15]]
        },
        {//2
            name: 'Acorn',
            imgSrc: './img/acorn.png',
            descr: 'Stabilizes at 5206th iteration. A methuselah (small pattern that stabilizes only after a long time). found by Charles Corderman',
            zoom: 3,
            speed: 10,
            fieldShift: { x: -72, y: -24 },
            set: [[35, 100], [36, 102], [37, 99, 100, 103, 104, 105]]
        },
        {//3
            name: 'Gosper Glider Gun',
            imgSrc: './img/g-gun.png',
            descr: 'The first known gun, and indeed the first known finite pattern with unbounded growth, found by Bill Gosper in November 1970. It remains by far the smallest known gun.',
            zoom: 1,
            speed: 20,
            fieldShift: { x: 0, y: 0 },
            set: [[23,68],[24,66,68],[25,56,57,64,65,78,79],[26,55,59,64,65,78,79],[27,44,45,54,60,64,65],[28,44,45,54,58,60,61,66,68],[29,54,60,68],[30,55,59],[31,56,57]]
        },
        {//4
            name: 'Line',
            imgSrc: './img/line.png',
            descr: 'Heavy pattern with fast growth',
            zoom: 4,
            speed: 20,
            fieldShift: { x: -85, y: -75 },
            set: [[30,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400]]
        },
        {//5
            name: 'Blinker Puffer',
            imgSrc: './img/b-puffer.png',
            descr: 'Was found by David Bell. The importance of this puffer is that the engine which produces the blinker output is only p4',
            zoom: 4,
            speed: 10,
            fieldShift: { x: -420, y: -80 },
            set: [[11,114,115,116],[12,113,114,115,116,117],[13,112,113,115,116,117],[14,113,114],[17,110,112],[18,103,109,112],[19,102,103,104,105,106,110,112],[20,101,102,106,107,109,110],[21,102,110],[22,103,104,107,110],[23,111],[24,103,104,107,110],[25,102,110],[26,101,102,106,107,109,110],[27,102,103,104,105,106,110,112],[28,103,109,112],[29,110,112],[32,113,114],[33,112,113,115,116,117],[34,113,114,115,116,117],[35,114,115,116]]
        },
        {//6
            name: 'Coe Ship',
            imgSrc: './img/c-ship.png',
            descr: 'A puffer engine discovered by Tim Coe in October 1995',
            zoom: 2,
            speed: 20,
            fieldShift: { x: 72, y: -24 },
            set: [[16,87,88,89,90,91,92],[17,85,86,92],[18,83,84,86,92],[19,87,91],[20,89],[21,89,90],[22,88,89,90,91],[23,88,89,91,92],[24,90,91]]
        },
        {//7
            name: 'Gliders by the dozen',
            imgSrc: './img/g-dozen.png',
            descr: 'Is a methuselah with a lifespan of 184 generations discovered sometime before September, 1971 by Roger H. Rosenbaum. Produces 12 escaped gliders and still life.',
            zoom: 4,
            speed: 5,
            fieldShift: { x: 0, y: 0 },
            set: [[103,260,263,264],[104,260,264],[105,260,261,264]]
        },
        {//8
            name: 'Eureka',
            imgSrc: './img/eureka.png',
            descr: 'Is a period 30 pre-pulsar shuttle oscillator in which two 15-generation pre-pulsar shuttle mechanisms each composed of two tubs hassle a pre-pulsar. It was found by David Buckingham in August 1980.',
            zoom: 0,
            speed: 20,
            fieldShift: { x: -35, y: -5 },
            set: [[14,20,35],[15,19,21,26,34,36],[16,20,24,25,27,28,35],[17,26],[21,26],[22,20,24,25,27,28,35],[23,19,21,26,34,36],[24,20,35]]
        },
        {//9
            name: 'Spacefiller',
            imgSrc: './img/spacefiller.png',
            descr: 'Is a pattern that grows at a quadratic rate by filling the plane with an agar (pattern covering the whole plane that is periodic in both space and time). Heavy pattern',
            zoom: 4,
            speed: 0,
            fieldShift: { x: -240, y: -80 },
            set: [[19,40,41,42,46,47,48],[20,39,42,46,49],[21,20,21,22,23,42,46,65,66,67,68],[22,20,24,42,46,64,68],[23,20,29,42,46,59,68],[24,21,24,27,28,31,57,60,61,64,67],[25,26,32,40,41,42,46,47,48,56,62],[26,26,32,41,47,56,62],[27,26,32,41,42,43,44,45,46,47,56,62],[28,21,24,27,28,31,34,35,40,48,53,54,57,60,61,64,67],[29,20,29,33,34,39,40,41,42,43,44,45,46,47,48,49,54,55,59,68],[30,20,24,34,35,53,54,64,68],[31,20,21,22,23,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,65,66,67,68],[32,36,38,50,52],[33,39,40,41,42,43,44,45,46,47,48,49],[34,39,49],[35,40,41,42,43,44,45,46,47,48],[36,44],[37,40,41,42,46,47,48],[38,42,46],[40,41,42,43,45,46,47],[41,41,42,43,45,46,47],[42,40,42,43,45,46,48],[43,40,41,42,46,47,48],[44,41,47]]
        }
    ],
       
    counters:
    {
        population: 0,  // current population
        iteration: 0,   // current iteration of simulation
        maxPop: 0,      // maximal reached population
        speed:          // measured algorithm and canvas perfomance speed (in ms)
        {
            algorithm: 0,
            canvas: 0,
            total: 0
        }
    },
    
    // position
    fieldShift: { x: 0, y: 0 },
        
    // cell sizes setups
    CellZoomSizes: [ 8, 6, 4, 3, 1 ], // 0 - smallest, 4 - largest
    
    //- Inits
    initEvents: function()
    {
        var lx = null,
            ly = null, // keeping last drawn coords to prevent multiple toggle on mouseover over the same cell [happens when drawing mode is set to 1]
        
        // helper function for getting cell coordinates from mouse positon relatively to canvas and drawing cell according to draw mode
        draw = function(e)
        {
            var offset = $('#game-field').offset(),
                x = Math.ceil((e.pageX - offset.left) / (GoL.CellZoomSizes[GoL.parameters.zoom] + 1)) - 1,
                y = Math.ceil((e.pageY - offset.top)  / (GoL.CellZoomSizes[GoL.parameters.zoom] + 1)) - 1;
            
            if (x !== lx || y !== ly)
            {
                switch(GoL.parameters.drawMode)
                {
                    case 0: GoL.canvas.changeCell(x, y, false, true); break; // draw
                    case 1: GoL.canvas.changeCell(x, y, true);        break; // toggle
                    case 2: GoL.canvas.changeCell(x, y, false, false); break;// erase
                    default: return;
                }
                lx = x; ly = y;
            }
        },
        
        md = false; // mousedown indicator for mouseover
        
        // canvas events
        $("#game-field").mousedown(function(e)
        {
            md = true;
            draw(e);
        });
        
        $("#game-field").mousemove(function(e)
        {
            if (md === false) return;
            draw(e);
        });
                
        $(document).mouseup(function()
        { 
            md = false;
            lx = ly = null; // reset last toggled element
        })
    
        // keydown events
        $(window).keydown(function(e)
        {
            if(!$('#game-speed').is(':focus')) // prevent keycode events when on speed field
            {
                var hit = true;
                switch(e.keyCode) {
                    case 37:  GoL.setShift('left');   break; // arrow left
                    case 38:  GoL.setShift('up');     break; // arrow up
                    case 39:  GoL.setShift('right');  break; // arrow right
                    case 40:  GoL.setShift('down');   break; // arrow down
                    case 67:  GoL.clear();            break; // c
                    case 68:  GoL.setDrawMode(0);     break; // d
                    case 69:  GoL.setDrawMode(2);     break; // e
                    case 70:  GoL.verticalResize();   break; // f
                    case 71:                                 // g
                        GoL.canvas.setGrid($('#g-1').hasClass('active'), true);
                    break;
                    case 80:  GoL.pause();            break; // p
                    case 82:  GoL.run();              break; // r
                    case 83:  GoL.runOnce();          break; // s
                    case 84:  GoL.setDrawMode(1);     break; // t
                    case 88:  GoL.setShift('reset');  break; // x
                    case 189: GoL.setZoom('out');     break; // - 
                    case 187: GoL.setZoom('in');      break; // = (+)
                    case 219: GoL.changeSpeed(false); break; // [
                    case 221: GoL.changeSpeed(true);  break; // ]
                    default: hit = false;
                }
            }
            if (hit) e.preventDefault();
        });
        
        var $wrapper = $('#canvas-wrapper'),
            w = $wrapper.width();
            
        // event on window resize to recalculate canvas width
        $(window).resize(function()
        {
            if (w != $wrapper.width())
            {
                w = $wrapper.width();
                GoL.canvas.init();
            }
        });
        
        // hide info on templates modal close
        $('#modal-templates').on('hide.bs.modal', function () {
            $('.t-descr.visible').animate({bottom: '-200px', opacity: '0'}, 140, function() { $(this).removeClass('visible'); });
        })
    },
    
    interval: null, // for cont. run
    
    // initializes the game
    init: function(r)
    {
        r = r || false;
        this.ListLife.init();
        this.loadTemplate();
        this.canvas.init();
        if (!r) this.initEvents();
    },  
    
    // resets all counters
    resetCounters: function()
    {
        this.counters.iteration = this.counters.population = this.counters.maxPop = 
        this.counters.speed.canvas = this.counters.speed.algorithm = this.counters.speed.total = 0;
    },
    
    // loads template if is set and proper
    loadTemplate: function()
    {
        if (this.parameters.template !== -1 && this.templates[this.parameters.template])
        {
            (function(scope)
            {
                GoL.ListLife.currentList = [];
                for (var i = 0; i < scope.set.length; i++)
                {
                    GoL.ListLife.currentList[i] = [];
                    for (var j = 0; j < scope.set[i].length; j++)
                    {
                        GoL.ListLife.currentList[i][j] = scope.set[i][j];
                        GoL.counters.population++;
                    }
                    GoL.counters.population--; // remove double count for first y-coord in set
                }
                GoL.setZoom('', scope.zoom);
                GoL.parameters.speed = scope.speed;
                GoL.fieldShift.x = scope.fieldShift.x;
                GoL.fieldShift.y = scope.fieldShift.y;
                $('#game-speed').val(scope.speed);
                GoL.printData();
            }
            )(this.templates[this.parameters.template]);
        }
    },
    
    // forwards simulation once
    simulateNextDay: function()
    {
        this.counters.iteration++;
        var t = new Date();
        this.counters.population = this.ListLife.next();
        this.counters.speed.algorithm = (new Date() - t);
        
        if (this.counters.population > this.counters.maxPop)
            this.counters.maxPop = this.counters.population;
       
        t = new Date();
        for (var i = 0; i < this.ListLife.drawList.length; i++)
        {
            this.canvas.setCell(this.ListLife.drawList[i][0] - GoL.fieldShift.x, this.ListLife.drawList[i][1] - GoL.fieldShift.y, this.ListLife.drawList[i][2] === 1);
        }
        
        this.counters.speed.canvas = (new Date() - t);
        this.counters.speed.total = this.counters.speed.canvas + this.counters.speed.algorithm;
        this.printData();
    },
    
    //- Controls
    
    // Runs simulation, taking speed parameter from the input
    run: function()
    {
        var $field = $("#game-field"),
            sp = $("#game-speed").val();
        if ($.isNumeric(sp) && sp >= 0) // min speed is 0ms
        {
            if (sp != this.parameters.speed)
            {
                this.parameters.speed = sp;
                this.pause();
            }
            $("#game-speed-wrap").removeClass('has-error');
        }
        else 
        {
            $("#game-speed-wrap").addClass('has-error');
            return;
        }

        if (!$field.hasClass('running'))
        {
            $("#g-run").addClass('active');
            $field.addClass('running');
           
            this.interval = setInterval(function()
            {
                GoL.simulateNextDay();
                if (GoL.counters.population == 0) GoL.pause();
            }, this.parameters.speed);
        }
    },
    
    // forwards simulation once
    runOnce: function()
    {
        this.pause();
        this.simulateNextDay();
    },
    
    // pauses simulation
    pause: function()
    {
        var $field = $("#game-field");
        if ($field.hasClass('running'))
        {
            $("#g-run").removeClass('active');
            $field.removeClass('running');
            clearInterval(this.interval);
            this.printData();
        }
    }, 
    
    // clears field and resets counters
    clear: function()
    {
        this.pause();
        this.setShift('reset');
        this.init(true);
        this.resetCounters();
        this.printData();
    },
    
    // sets drawing mode: 0 - draw, 1 - toggle, 2 - erase
    setDrawMode: function (m)
    {
        if (m === 0 || m === 1 || m === 2)
        {
            this.parameters.drawMode = m;
            $('.dm').removeClass('active');
            $('#dm-' + m).addClass('active');
        }
    },
    
    // sets field shift [position of observable field]
    setShift: function(act)
    {
        var s = this.fieldShift,
            offset = (GoL.parameters.zoom + 1) * 4;
            
        switch(act)
        {
            case 'up':
            s.y += offset;
            break;
            case 'down':
            s.y -= offset;
            break;
            case 'left':
            s.x += offset;
            break;
            case 'right':
            s.x -= offset;
            break;
            case 'reset':
            s.x = s.y = 0;
            break;
            default: return;
        }
        this.canvas.cleanField();
        this.canvas.renderField();
    },
    
    changeSpeed: function(inc)
    {
        var speed = parseInt($("#game-speed").val(), 10);
        speed = inc ? speed + 5 : speed - 5;
        speed = speed < 0 ? 0 : speed;
        $("#game-speed").val(speed);
        if ($("#game-field").hasClass('running')) this.run();
    },
    
    // Sets zoom
    setZoom: function(act, value)
    {
        var $buttonIn = $('#z-in'),
            $buttonOut = $('#z-out'),
            d = 'disabled';
        
        if (act.length)
        {
            switch(act)
            {
                case 'out':
                    if (GoL.parameters.zoom < 4) GoL.parameters.zoom++;
                break;
                case 'in':
                    if (GoL.parameters.zoom > 0) GoL.parameters.zoom--;
                break;
                default: return;
            }
        }
        else
        {
            if (value < 5 && value >= 0)
            {
                GoL.parameters.zoom = value;
            }
        }
      
        if (GoL.parameters.zoom === 0)
            $buttonIn.attr(d, d);
        else
            $buttonIn.removeAttr(d);
        
        if (GoL.parameters.zoom === 4)
            $buttonOut.attr(d, d);
        else
            $buttonOut.removeAttr(d);
          
        this.canvas.init();
    },
    
    // toggles fullheight mode 
    verticalResize: function() {
        if ((($(window).height() - 40) > 440) && GoL.parameters.wpx > 768)
        {
            this.parameters.fullHeight = !this.parameters.fullHeight;
            this.canvas.init();
            $('#v-resize span').toggleClass('glyphicon-arrow-down').toggleClass('glyphicon-arrow-up')
        }
        else
        {
            $('#v-resize').fadeOut(200);
            this.parameters.fullHeight = false;
            alert('Unfortunately, your screen size doesn\'t allow to increase canvas height');
        }
    },
    
    // sets template according to id. does nothing if id is invalid
    setTemplate: function(id)
    {
        if (($.isNumeric(id) && this.templates[id]) || id === -1)
        {
            if (id === this.parameters.template) // if same button clicked again -> unselect
            {
                id = -1;
            }
            this.pause();
            this.parameters.template = id;
            $('.t').removeClass('active').html('Select');
            $('.t-img').removeClass('green-border');
            if (id !== -1) {
                $('#t-' + id).addClass('active').html('Unselect');
                $('#t-img-' + id).addClass('green-border');
                $('#g-templates').addClass('active');
            }
            else
            {
                $('#g-templates').removeClass('active');
            }
        }
        this.resetCounters();
        this.printData();
        this.init(true);
    },

    // updates counters on the page   
    printData: function()
    {
        $('#population').html(this.counters.population);
        $('#iteration').html(this.counters.iteration);
        $('#max-pop').html(this.counters.maxPop);
        // speeds
        $('#ca-speed').html(this.counters.speed.canvas);
        $('#al-speed').html(this.counters.speed.algorithm);
        $('#tt-speed').html(this.counters.speed.total).removeClass('text-danger', 'text-success').addClass(this.counters.speed.total > this.parameters.speed ? 'text-danger' : 'text-success');
    }, 
    
    // prints templates on the page (and version)
    printTemplates: function()
    {
        var $wrap = $('#templates-body'),
            HTML = '';
        if (this.templates.length)
        {
            for (var i = 0; i < this.templates.length; i++)
            {
                if (i % 4 == 0) 
                {
                    if (i !== 0) HTML += '</div>';
                    HTML += '<div class="row">'
                }
                HTML += '<div class="col-xs-3">';
                
                HTML += '<h4>' + this.templates[i].name + '</h4>';
                HTML += '<div class="t-d-wrap"><img id="t-img-' + i + '" src="' + this.templates[i].imgSrc + '"class="t-img img-responsive img-thumbnail"><div class="t-descr" id="t-descr-' + i + '">' + this.templates[i].descr + '</div></div>';
                //HTML +='<p></p>';
                HTML += '<div class="btn-group btn-group-justified">\
                    <a type="button" role="button" id="t-' + i + '" class="t btn btn-sm btn-default" data-dismiss="modal" onclick="GoL.setTemplate(' + i + ');">Select</a>\
                    <a type="button" role="button" class="t-info btn btn-sm btn-default" onclick="GoL.templateShowDescr(' + i + ')">Info</a>\
                </div></div>';
                if (i == this.templates.lenght) HTML += '</div>'
            }
        }
        // templates print
        $wrap.html(HTML);
        // version print
        $('#game-version').html('v ' + this.parameters.v);
    },
    
    templateShowDescr: function(id)
    { 
        var speed = 140;
        function toggle(obj)
        {
            var hide = $(obj).hasClass('visible');
            $(obj).stop().animate
            (
                {
                    bottom: hide ? '-200px' : '0',
                    opacity: hide ? '0' : '1'
                },
                speed,
                function()
                {
                    $(this).toggleClass('visible')
                }
            );
        }
        toggle('.t-descr.visible');
        toggle('#t-descr-' + id);
    },
    
    // GUI
    canvas:
    {
        ctx: null,
        grid: [],
        cellSize: 0,
        cellSpace: 0,

        init: function()
        {
            var $field = $('#game-field');
            this.ctx =  $field[0].getContext('2d');
            this.cellSize = GoL.CellZoomSizes[GoL.parameters.zoom];
            
             // disable grid automatically when zoom is to small with no reRender
            this.setGrid(!(GoL.parameters.zoom === 4), false);
            
            // set height according to fullheight mode
            GoL.parameters.hpx = GoL.parameters.fullHeight ? $(window).height() - 40 : 440;
            
            // set width according to wrapper width (minus borders)
            GoL.parameters.wpx = $("#canvas-wrapper").width() - 2;
            
            // hide button if screen width is too small
            GoL.parameters.wpx < 769 ? $("#v-resize").fadeOut(200) : $("#v-resize").fadeIn(200);
            
            // calculate amount of cells vertically and horizontally according canvas size
            GoL.parameters.h = Math.ceil(GoL.parameters.hpx / (this.cellSize + this.cellSpace));
            GoL.parameters.w = Math.ceil(GoL.parameters.wpx / (this.cellSize + this.cellSpace));
            
            // set canvas size
            $field.attr('height', GoL.parameters.hpx).attr('width', GoL.parameters.wpx);          
            
            if (GoL.parameters.fullHeight && $('body').scrollTop() < 80) // prevent scrolling if not at the top
                $('body').scrollTop(80);
            
            this.cleanField();
            this.renderField();
        },
        
        setGrid: function(flag, reRender)
        {
            $('.g').removeClass('active');
            if (flag)
            {
                this.cellSpace = 1;
                this.cellSize = GoL.CellZoomSizes[GoL.parameters.zoom];
                $('#g-0').addClass('active');
            }
            else
            {
                this.cellSpace = 0;
                this.cellSize = GoL.CellZoomSizes[GoL.parameters.zoom] + 1;
                $('#g-1').addClass('active');
            }
            if (reRender) this.renderField();
        },

        cleanField: function ()
        {
            this.grid = []; // reset/init grid
        
            for (var i = 0 ; i < GoL.parameters.w; i++)
            {
                this.grid[i] = [];
                for (var j = 0 ; j < GoL.parameters.h; j++)
                {
                    this.grid[i][j] = 0; // all cells are set to dead
                }
            }
        },

        renderField: function()
        {
            // fill bg [grid]
            this.ctx.fillStyle = '#f6f6f6';
            this.ctx.fillRect(0, 0, GoL.parameters.wpx, GoL.parameters.hpx); // 0, 0, maxwidth, maxheight
            
            // draw all cells
            for (var i = 0 ; i < GoL.parameters.w; i++)
            {
                for (var j = 0 ; j < GoL.parameters.h; j++)
                {
                    this.renderCell(i, j, GoL.ListLife.isAlive(i + GoL.fieldShift.x, j + GoL.fieldShift.y));
                }
            }
        },
        
        // cells operations
        renderCell: function (x, y, isAlive)
        {
            this.ctx.fillStyle = isAlive ? '#ccc' : '#fff';
            this.ctx.fillRect(
                (this.cellSpace + this.cellSize) * x,
                (this.cellSpace + this.cellSize) * y,
                this.cellSize,
                this.cellSize
            );
        },
        
        // changes Cell according to the setup: if toggle -> toggles cell, else sets cell according to the givenStatus. if currentStatus is same as given -> no action
        changeCell: function(x, y, toggle, givenStatus)
        {
            var currentStatus = GoL.ListLife.isAlive(x + GoL.fieldShift.x, y + GoL.fieldShift.y);

            if ((toggle && currentStatus) || (!toggle && !givenStatus && currentStatus))
            {
                this.setCell(x, y, false);
                GoL.ListLife.removeCell(x + GoL.fieldShift.x, y + GoL.fieldShift.y, GoL.ListLife.currentList);
                GoL.counters.population--;
            }
            else if ((toggle && !currentStatus) || (!toggle && givenStatus && !currentStatus))
            {
                this.setCell(x, y, true);
                GoL.ListLife.addCell(x + GoL.fieldShift.x, y + GoL.fieldShift.y, GoL.ListLife.currentList);
                GoL.counters.population++;
            }
            GoL.printData();
        },
  
        setCell: function (x, y, isAlive)
        {
            if (x >= 0 && x < GoL.parameters.w && y >= 0 && y < GoL.parameters.h)
            {
                this.grid[x][y] = isAlive ? 1 : 0;
                this.renderCell(x, y, isAlive);
            }
        }
    },
    
    // following algorithm (idea of exact implementation) is taken from http://dotat.at/prog/life/life.html
    ListLife:
    {
        currentList: [],
        drawList: [],
        
        init: function() {
            this.currentList = [];
        },
        
        next: function() {
            this.drawList = [];
            
            var check = {}, // list of dead cells to be checked
                newList = [], 
                population = 0;
            
            for (var i = 0; i < this.currentList.length; i++)
            {
                this.topPtr = 1;
                this.bottomPtr = 1;
               
                for (var j = 1; j < this.currentList[i].length; j++)
                {
                    var x = this.currentList[i][j],
                        y = this.currentList[i][0];
                    
                    var deadAround = [ // generating pesudo dead 3x3 surrounding, alive cells to be replaced
                        [x-1, y-1, 1],  [x, y-1, 1],  [x+1, y-1, 1],
                        [x-1,  y,  1], /* x, y, 1 */  [x+1,  y,  1],
                        [x-1, y+1, 1],  [x, y+1, 1],  [x+1, y+1, 1]
                    ];
                    
                    // replacing alive cells and getting amount replaced
                    var aliveAmount = this.replaceAlive(x,  y,   i,  deadAround);
                       
                    for (var k = 0; k < 8; k++) // iterating through real deadAround
                    {
                       if (deadAround[k] !== undefined) // if neighbour is alive
                       {
                           var key = deadAround[k][0] + ',' + deadAround[k][1]; // x, y
                           
                           check[key] = (check[key] === undefined) ? 1 : check[key] + 1;
                        }
                    }

                    // life rules check: 
                    if (!(aliveAmount === 0 || aliveAmount === 1 || aliveAmount > 3)) 
                    {
                        this.addCell(x, y, newList);
                        this.drawList.push([x, y, 1]); // keep alive
                        population++;
                    }
                    else
                    {
                        this.drawList.push([x, y, 0]); // kill
                    }
                }
            }

            for (key in check)
            {
                if (check[key] === 3) // add new alive
                { 
                    var spl = key.split(',');
                    
                    x = parseInt(spl[0], 10),
                    y = parseInt(spl[1], 10);
                        
                    this.addCell(x, y, newList);
                    this.drawList.push([x, y, 1]);
                    population++;
                }
            }

            this.currentList = newList;
            return population;
        },
        
        replaceAlive: function(x, y, i, around)
        {
            return this.replaceAliveFromRow(x, y-1, i-1, around, 0) + 
                   this.replaceAliveFromRow(x,  y,   i,  around, 3) +
                   this.replaceAliveFromRow(x, y+1, i+1, around, 5);
        },
        
        replaceAliveFromRow: function (x, y, i, around, offset)
        {
            var list = this.currentList; // shortcut
            var result = 0;
            if (list[i] !== undefined || offset === 3)
            {
                if (list[i][0] === y || offset === 3)
                {
                    for (var j = 1; j < list[i].length; j++)
                    {
                        if (list[i][j] >= (x - 1))
                        {
                            if (list[i][j] === (x - 1))
                            {
                                delete around[0 + offset];
                                result++;
                            }
                            
                            if (list[i][j] === x &&  offset !== 3)
                            {
                                delete around[1 + offset];
                                result++;
                            }
                            
                            if (list[i][j] === (x + 1))
                            {
                                delete around[((offset !== 3) ? 2 : 1) + offset];
                                result++;
                            }
                            
                            if (list[i][j] > (x + 1))
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return result;
        },
        
        isAlive: function(x, y)
        {
            for (var i = 0; i < this.currentList.length; i++)
            {
                if (this.currentList[i][0] === y)
                {
                    for (var j = 1; j < this.currentList[i].length; j++)
                    {
                        if (this.currentList[i][j] === x)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        },
        
        removeCell: function(x, y, list)
        {
            for (var i = 0; i < list.length; i++)
            {
                if (list[i][0] === y)
                {
                    if (list[i].length === 2) // if single element -> remove row
                    {
                        list.splice(i, 1);
                    }
                    else // remove particular element from row
                    {
                        for (var j = 1; j < list[i].length; j++)
                        {
                            if (list[i][j] === x)
                            {
                                list[i].splice(j, 1);
                            }
                        }
                    }
                }
            }
        },
        
        addCell: function(x, y, list)
        {
            if (list.length === 0)
            {
                list.push([y, x]);
                return;
            }
            
            var tmp;
            
            if (y < list[0][0])  // add to head
            {
                tmp = [[y, x]];
                for (var i = 0; i < list.length; i++)
                {
                    tmp[i+1] = list[i];
                }

                for (i = 0; i < tmp.length; i++)
                {
                    list[i] = tmp[i];
                }
                return;
            }
            else if (y > list[list.length - 1][0]) // add to end
            { 
                list[list.length] = [y, x];
                return;
            }
            else // add in the middle
            { 
                for (var i = 0; i < list.length; i++)
                {
                    if (list[i][0] === y) // add to existing row
                    { 
                        var added = false;
                        tmp = [];
                        for (var j = 1; j < list[i].length; j++)
                        {
                            if ((!added) && (x < list[i][j]))
                            {
                                tmp.push(x);
                                added = !added;
                            }
                            tmp.push(list[i][j]);
                        }
                        tmp.unshift(y);
                        if (!added)
                        {
                            tmp.push(x);
                        }
                        list[i] = tmp;
                        return;
                    }

                    if (y < list[i][0]) // create row and add to it
                    { 
                        tmp = [];
                        for (var j = 0; j < list.length; j++)
                        {
                            if (i === j)
                            {
                                tmp[j] = [y, x];
                                tmp[j+1] = list[j];
                            }
                            else if (i > j)
                            {
                                tmp[j] = list[j];
                            }
                            else if (i < j)
                            {
                                tmp[j+1] = list[j];
                            }
                        }

                        for (j = 0; j < tmp.length; j++)
                        {
                            list[j] = tmp[j];
                        }
                        return;
                    }
                }
            }
        }
    }
};
