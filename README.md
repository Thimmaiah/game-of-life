# Conway's Game of Life #

This is JavaScript implementation of Conway's Game of Life. **[Live example](http://mion.elka.pw.edu.pl/~dievseie/GOL/v2/)**

### Libraries used ###

* Bootstrap 3
* jQuery

### Algorithm ###

* Idea of algorithm is taken from [Tony Finch's](http://dotat.at/) [List Life](http://dotat.at/prog/life/life.html)

### Useful links ###

* [Wiki](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
* [Lexicon](http://www.bitstorm.org/gameoflife/lexicon/)

#### Current version ####

* 2.9.3